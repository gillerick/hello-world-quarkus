# Hello World

A simple Quarkus API that returns greetings in French and English depending on the endpoint hit

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

### Endpoints
GET: http://127.0.0.1:8080/hello/fr

GET: http://127.0.0.1:8080/hello/en

### Demo

![en](assets/demo/en.JPG)
English Greeting Endpoint

![fr](assets/demo/en.JPG)
French Greeting Endpoint


