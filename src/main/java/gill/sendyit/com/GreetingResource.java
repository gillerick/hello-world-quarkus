package gill.sendyit.com;

import gill.sendyit.com.gateway.Endpoints;
import io.quarkus.vertx.web.Route;
import io.quarkus.vertx.web.RouteBase;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@RouteBase(path = Endpoints.ROUTE_BASE)
public class GreetingResource {

    @GET
    @Route(path = Endpoints.GREETING_EN)
    @Produces(MediaType.APPLICATION_JSON)
    public String helloEng() {
        return "Hello";
    }

    @GET
    @Route(path = Endpoints.GREETING_FR)
    @Produces(MediaType.APPLICATION_JSON)
    public String helloFre() {
        return "Bonjour";
    }
}