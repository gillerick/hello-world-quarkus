package gill.sendyit.com.gateway;

public class Endpoints {
    public static final String ROUTE_BASE = "/hello";
    public static final String GREETING_EN = "/en";
    public static final String GREETING_FR = "/fr";
}
